# Roger the Omni Grader

Roger is an automated grading system for R scripts... and much more.
The system is based on a set of Unix shell scripts to grade the
results of student projects, and an R package to check the coding
style and documentation of R scripts.

Roger was developed with the [Computer Science Program Grading
Criteria](https://wiki.cs.astate.edu/index.php?title=Computer_Science_Program_Grading_Criteria&oldid=10507)
of Arkansas State University in mind, but the system is very flexible
and it can easily be adapted to other contexts. As long as you have
grading criteria and a number of points attached, you should be good
to go. You do have grading criteria, right?

Roger is actively used for grading at [École
d'actuariat](https://act.ulaval.ca) of [Université
Laval](https://ulaval.ca).

## R package

The R package **roger** provides functions to check the style and
documentation of R scripts. It also features an interface to the shell
scripts of the [base system](https://roger-project.gitlab.io).

The package was inspired by, and is partly based on,
[**lintr**](http://cran.r-project.org/package=lintr).

## Authors

David Beauchemin was the first to write a few scripts to accelerate
grading of 100+ projects many times per semester. Jean-Christophe
Langlois wrote the first version of the R package. Samuel Fréchette
wrote the R interface to the shell scripts and unit tests. Vincent
Goulet wrote the shell scripts and is the maintainer of the project.
