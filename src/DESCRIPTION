Package: roger
Type: Package
Title: Automated Grading of R Scripts
Version: 1.5-1
Date: 2023-10-23
Authors@R: c(person("Vincent", "Goulet", role = c("aut", "cre"),
                    email = "vincent.goulet@act.ulaval.ca"),
             person("Samuel", "Fréchette", role = "aut"),
             person("Jean-Christophe", "Langlois", role = "aut"),
             person("Jim", "Hester", role = "ctb"))
Description: Tools for grading the coding style and documentation of R
  scripts. This is the R component of Roger the Omni Grader, an
  automated grading system for computer programming projects based on
  Unix shell scripts; see <https://gitlab.com/roger-project>. The
  package also provides an R interface to the shell scripts. Inspired by
  the lintr package.
Depends: R (>= 4.0.0)
Imports: utils
Suggests: tinytest
SystemRequirements: roger-base (for interface functions only)
License: GPL (>=2)
URL: https://roger-project.gitlab.io
BugReports: https://gitlab.com/roger-project/roger-rpkg/-/issues
Encoding: UTF-8
